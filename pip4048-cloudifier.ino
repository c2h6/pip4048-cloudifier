#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <WiFiClientSecure.h>
#include "credentials.h"

#define BATT_LOW_THRESHOLD 48
#define BATT_OK_THRESHOLD 49

const String      telegramApiKey            = TELEGRAM_API_KEY;

float gridVolt, gridFreq, outVolt, outFreq, battVolt, pvVolt, scc, battBalance;
int loadVA, loadW, loadPct, busVolt, battChg, battSOC, tempHeatsink, pvCurrent, battDisch, battCurrent, pvPow;
const byte cmdQPIGS [8]= {0x51,0x50,0x49,0x47,0x53,0xB7,0xA9,0x0D}; // include CR as delimiter
String reply = "", devStatus1, vOffsetFans, eepromVersion, devStatus2;
bool configStatusChanged, SCCfirmwareUpdated, loadOn, chargingNow,
  chargingFromPV, chargingFromAC, chargingFloat, switchOn;
int battLowTicks = 0;
float oldGridVolt = 0;
bool chargeStartNotified, chargeStopNotified, battLowNotified, battOkNotified;

ESP8266WiFiMulti wifiMulti;
WiFiClientSecure secureClient;
WiFiClient client;

void sendTelegram(String chat_id, String text) {

  if(!secureClient.connected()){
    secureClient.connect("api.telegram.org", 443);
  }

  String postStr = "{\"chat_id\": \"" + chat_id + "\",\"text\": \"" + text + "\"}\r\n\r\n";
  
  secureClient.print("POST /bot"+telegramApiKey+"/sendMessage HTTP/1.1\n");
  secureClient.print("Host: api.telegram.org\n");
  secureClient.print("Content-Type: application/json\n");
  secureClient.print("Connection: close\n");
  secureClient.print("Content-Length: ");
  secureClient.print(postStr.length());
  secureClient.print("\n\n");
  secureClient.print(postStr);
  
  return;
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // LED off
  pinMode(D4, OUTPUT);
  digitalWrite(D4, HIGH); // LED off
  delay(100);
  WiFi.mode(WIFI_STA);
  wifiMulti.addAP(WIFI_SSID_1, WIFI_PASS_1);
  wifiMulti.addAP(WIFI_SSID_2, WIFI_PASS_2);
  wifiMulti.addAP(WIFI_SSID_3, WIFI_PASS_3);

  int i=0;
  while (wifiMulti.run() != WL_CONNECTED) {
    delay(1000);
    if (i++ > 30)
      ESP.restart();
  }

  Serial.setTimeout(1000); // 1000ms = default timeout for readStringUntil

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }
    sendTelegram(TELEGRAM_CHAT_ID, "Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    sendTelegram(TELEGRAM_CHAT_ID, "End, rebooting...");
    delay(1000);
  });

  ArduinoOTA.onError([](ota_error_t error) {
//    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      sendTelegram(TELEGRAM_CHAT_ID, "Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      sendTelegram(TELEGRAM_CHAT_ID, "Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      sendTelegram(TELEGRAM_CHAT_ID, "Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      sendTelegram(TELEGRAM_CHAT_ID, "Receive Failed");
    } else if (error == OTA_END_ERROR) {
      sendTelegram(TELEGRAM_CHAT_ID, "End Failed");
    }
  });

  ArduinoOTA.begin();

  sendTelegram(TELEGRAM_CHAT_ID, "Connected To Wifi " + WiFi.SSID() + " at "
    + WiFi.localIP().toString() + ", starting main loop");

} // setup

boolean queryPip() {
  digitalWrite(LED_BUILTIN, LOW); // LED on
  Serial.write(cmdQPIGS,8);
  Serial.flush();
  delay(50);
  digitalWrite(LED_BUILTIN, HIGH); // LED off
  
  reply = Serial.readStringUntil('\n');
  if (reply.length() > 1) {
    digitalWrite(D4, LOW); // LED on
    delay(20);
    digitalWrite(D4, HIGH); // LED off
  }
//           gridV Hz   outV  Hz   VA   W    ld% bus batV  chg SOC temp pvCu pvVo  sccV  disch devStat1 of ee pvPow devStat2
// Sample: "(000.0 00.0 230.0 50.0 0069 0025 001 424 52.50 002 100 0044 0002 072.6 52.51 00000 00010110 00 00 00152 010Wp"  
  sscanf(reply.c_str(), "(%f %f %f %f %d %d %d %d %f %d %d %d %d %f %f %d %s %s %s %d %s",
    &gridVolt, &gridFreq, &outVolt, &outFreq, &loadVA, &loadW, &loadPct, &busVolt,
    &battVolt, &battChg, &battSOC, &tempHeatsink, &pvCurrent, &pvVolt, &scc, &battDisch,
    &devStatus1, &vOffsetFans, &eepromVersion, &pvPow, &devStatus2);
  battCurrent = battChg - battDisch;
  battBalance += battCurrent * battVolt * 6/3600; // Battery charge/discharge in Wh, TODO: calculate more precise by clocking actual timespan
   // devStatus1[0] "add SBU priority version" ??
  configStatusChanged = (devStatus1[1]=='1');
  SCCfirmwareUpdated = (devStatus1[2]=='1');
  loadOn = (devStatus1[3]=='1');
  // devStatus1[4] = "battery voltage to steady while charging" ??
  chargingNow = (devStatus1[5]=='1');
  chargingFromPV = (devStatus1[6]=='1');
  chargingFromAC = (devStatus1[7]=='1');
  chargingFloat = (devStatus2[0]=='1');
  switchOn = (devStatus2[1]=='1');

  return (battVolt != 0);
}

void updateMyCloud() {
    if(!client.connected()){
      client.connect("c2h6.eu",8086);
    }
    String postStr = "DEADBEEF "; //TODO: replace with ESP MAC address
    postStr +="gridVolt="+String(gridVolt);
    postStr +=",pvPow="+String(pvPow);
    postStr +=",loadW="+String(loadW);
    postStr +=",battVolt="+String(battVolt);
    postStr +=",battCurrent="+String(battCurrent);
    postStr +=",tempHeatsink="+String(tempHeatsink);
    postStr +=",battBalance="+String(battBalance);
    postStr +=",busVolt="+String(busVolt);
    postStr +=",chargingFromAC="+String(chargingFromAC);
    postStr +=",gridFreq="+String(gridFreq);
    postStr += "\n\n";
 
    client.print("POST /write?db=newtest HTTP/1.1\n");
    client.print("Host: c2h6.eu\n");
    client.print("Connection: close\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("User-Agent: Ethan's ESP8266\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.print(postStr);
    client.stop();
}

void loop() {
  for (int i=0;i<50;i++) {    // run OTA handle every 100ms for ~6 seconds
    ArduinoOTA.handle();
    delay(100);  
  }

  boolean validData = queryPip(); //TODO: use timer instead

  if (WiFi.SSID()=="Maple Ranch") { // only in live production
    updateMyCloud();
  }

  if (battVolt > BATT_OK_THRESHOLD) {
    battLowTicks = 0;
    if (!battOkNotified)
      sendTelegram(TELEGRAM_CHAT_ID, "INFO: Battery OK, at "+String(battVolt)+" V");
      battOkNotified = true;
  }
    
  if (battVolt < BATT_LOW_THRESHOLD && !battLowNotified) {
    sendTelegram(TELEGRAM_CHAT_ID, "WARN: Battery at "+String(battVolt)+" V");
    battLowNotified = true;
  }

  if (gridVolt > 150 && oldGridVolt < 100) {
    sendTelegram(TELEGRAM_CHAT_ID, "INFO: Generator started");
    chargeStopNotified = false;
    chargeStartNotified = false;
  }

  if (gridVolt < 150 && oldGridVolt > 200) {
    sendTelegram(TELEGRAM_CHAT_ID, "INFO: Generator stopped");
    chargeStartNotified = false;
  }

  if (battCurrent > 10 && gridVolt > 100 && !chargeStartNotified) {
    sendTelegram(TELEGRAM_CHAT_ID, "INFO: Charging started OK");
    chargeStartNotified = true;
    chargeStopNotified = false;
  }
  
  if (battCurrent < 5 && gridVolt > 150 && !chargeStopNotified) {
    sendTelegram(TELEGRAM_CHAT_ID, "INFO: Charging stopped!");
    chargeStopNotified = true;
    chargeStartNotified = false;
  }

  oldGridVolt = gridVolt;
} // loop
