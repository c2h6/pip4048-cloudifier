sudo apt-get install docker.io

sudo usermod -a -G docker arnolde

docker run -d -p 127.0.0.1:8086:8086 -v /data/influxdb:/var/lib/influxdb influxdb:1.7-alpine

docker run -d -p 3000:3000 grafana/grafana

curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE DATABASE "pip4048"'

#Post Data:
curl -XPOST 'http://localhost:8086/write?db=dbname' --data-binary "TEST t=0.00,v=0.00"
