# esp8266-SolarLogger

## Indroduction

This is a firmware for an ESP8266 microcontroller with integrated Wifi to periodically query parameters (voltage, current, etc.) via serial interface (TTL) on solar inverters (like PIP, Effekta, Skymax, Voltronic, Infinisolar, Giant Power IPS, MPP Solar, etc.) that use the HS_MS_MSX RS232 Protocol.
It shall make queries as frequent as possible (every second?), buffer them locally and then periodically (i.e. every minute) upload the data to a cloud server via REST API.
The time shall be synchronized via NTP to provide reliable timestamps.

The necessary user configuration is limited to setting up the wifi credentials and setting the API endpoint.

I plan also to provide a docker container containing a node.js REST server, an sql database (sqlite or mysql), and a simple web visualization of the aggragated usage data (i.e. rrdtool or something similar).

## Hardware

The PIP4048 inverter has a standard RS232 interface (+/- 12V logic) which can be used for logging and configuration. Internally the comminication board is connected to the main controller board by 5V TTL logic on connector CN3.
The ESP8266 has a 3.3V TTL logic, the TX pin of which should drive the 5V RX pin ok, but we need to divide the 5V TX down to 3.3v so the 3.3V RX pin will not be damaged. I will attempt to do this with a simple resistor voltage dividing circuit (470Ohm/274Ohm) which I happen to have in stock. That should provide about 3.15V which should be enough for a reliable "high" level.
